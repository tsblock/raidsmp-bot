import discord
import config
from mcstatus import MinecraftServer
from discord.ext import tasks


class RaidSMPBot(discord.Client):
    async def on_ready(self):
        print("Ready.")

    async def on_message(self, message):
        mention = "<@!785086597766971392>"
        if mention in message.content:
            await message.channel.trigger_typing()
            await get_server_status_in_embed(message)
        pass


async def get_server_status_in_embed(ctx):
    server = MinecraftServer.lookup("raidsmp.youplonker.xyz:25586")
    try:
        status = server.status()
    except Exception:
        error_embed = discord.Embed(
            title="RaidSMP Status",
            color=discord.Color.red(),
            description="**Server is offline.**"
        )
        await ctx.channel.send(embed=error_embed)
    else:
        status_embed = discord.Embed(
            title="RaidSMP Status",
            color=discord.Color.green(),
        )
        status_embed.add_field(name="Version", value=status.version.name.replace("Paper ", ""))
        status_embed.add_field(name="Player Count", value=status.players.online)
        players = []
        if status.players.sample is not None:
            for player in status.players.sample:
                players.append(player.name)
            status_embed.add_field(name="Players", value="`{}`".format(", ".join(players)))
        await ctx.channel.send(embed=status_embed)


client = RaidSMPBot()
client.run(config.token)
